#!/bin/bash

# Loop through each mp3 file in the directory
for file in *.mp3; do
    # Check if the filename already contains '__DRLEN'
    if [[ "$file" == *"__DRLEN"* ]]; then
        echo "Skipping $file as it already contains '__DRLEN'"
        continue
    fi
    
    # Calculate the duration of the mp3 file in milliseconds
    duration=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$file" | awk '{printf "%.0f", $1}')
    
    # Get the filename without the extension
    filename="${file%.*}"
    
    # Get the extension
    extension="${file##*.}"
    
    # Create the new filename with the duration appended
    new_filename="${filename}__DRLEN_${duration}.${extension}"
    
    # Rename the file
    mv -n "$file" "$new_filename"
    
    echo "Renamed $file to $new_filename"
done

