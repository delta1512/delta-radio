// peer connection
var pc = null;


var audioContext = null;
var gainNode = null;


function getAudioContext() {
    return audioContext || new (window.AudioContext || window.webkitAudioContext)()
}


function getGainNode() {
    if (!gainNode) {
        let ac = getAudioContext();
        gainNode = ac.createGain();
        gainNode.connect(ac.destination);
    }
    return gainNode;
}


function setupVolumeControl() {
    var volumeSlider = document.getElementById("volume-slider");

    if (volumeSlider) {
        volumeSlider.addEventListener("input", function () {
            let gn = getGainNode()
            gn.gain.value = volumeSlider.value;
            localStorage.setItem('volume', volumeSlider.value);
        });
    }

    let gn = getGainNode()
    const vol = localStorage.getItem('volume');
    if (vol || vol == 0.0) {
        gn.gain.value = vol;
        volumeSlider.value = vol;
    }
}


function createPeerConnection() {
    var config = {
        sdpSemantics: 'unified-plan'
    };

    config.iceServers = [{ urls: ['stun:stun.l.google.com:19302'] }];

    pc = new RTCPeerConnection(config);

    // Create AudioContext
    if (!audioContext) {
        audioContext = new (window.AudioContext || window.webkitAudioContext)();
    }

    // Create GainNode
    if (!gainNode) {
        gainNode = audioContext.createGain();
        gainNode.connect(audioContext.destination);
    }

    pc.addEventListener('track', function (evt) {
        var source = audioContext.createMediaStreamSource(evt.streams[0]);
        source.connect(gainNode);
    });

    return pc;
}


function negotiate() {
    return pc.createOffer()
        .then(function (offer) {
            console.log('Step 1: Created offer');
            return pc.setLocalDescription(offer);
        })
        .then(() => {
            console.log('Step 2: Set local description');
            // wait for ICE gathering to complete
            return new Promise(function (resolve) {
                if (pc.iceGatheringState === 'complete') {
                    console.log('ICE gathering already complete');
                    resolve();
                } else {
                    function checkState() {
                        if (pc.iceGatheringState === 'complete') {
                            console.log('ICE gathering complete');
                            pc.removeEventListener('icegatheringstatechange', checkState);
                            resolve();
                        }
                    }
                    pc.addEventListener('icegatheringstatechange', checkState);
                }
            });
        })
        .then(() => {
            console.log('Step 3: ICE gathering is complete, proceed with the offer');
            var offer = pc.localDescription;
            var codec = "opus/48000/2";

            return fetch('/offer', {
                body: JSON.stringify({
                    sdp: sdpFilterCodec('audio', codec, offer.sdp),
                    type: offer.type
                }),
                headers: {
                    'Content-Type': 'application/json'
                },
                method: 'POST'
            });
        })
        .then((response) => {
            console.log('Step 4: Received response from server');
            return response.json();
        })
        .then((answer) => {
            console.log('Step 5: Set remote description with answer');
            return pc.setRemoteDescription(answer);
        })
        .catch((e) => {
            console.error('Error:', e);
            alert(e);
        });
}


function start(button) {
    button.disabled = true;
    pc = createPeerConnection();
    setupVolumeControl()

    const audioCtx = getAudioContext();
    const oscillator = audioCtx.createOscillator();
    const serverDestination = audioCtx.createMediaStreamDestination();
    oscillator.connect(serverDestination);

    const stream = new MediaStream();
    serverDestination.stream.getTracks().forEach((track) => {
        pc.addTrack(track, stream);
    })

    negotiate();

    audioCtx.resume();
}


function sdpFilterCodec(kind, codec, realSdp) {
    var allowed = []
    var rtxRegex = new RegExp('a=fmtp:(\\d+) apt=(\\d+)\r$');
    var codecRegex = new RegExp('a=rtpmap:([0-9]+) ' + escapeRegExp(codec))
    var videoRegex = new RegExp('(m=' + kind + ' .*?)( ([0-9]+))*\\s*$')

    var lines = realSdp.split('\n');

    var isKind = false;
    for (var i = 0; i < lines.length; i++) {
        if (lines[i].startsWith('m=' + kind + ' ')) {
            isKind = true;
        } else if (lines[i].startsWith('m=')) {
            isKind = false;
        }

        if (isKind) {
            var match = lines[i].match(codecRegex);
            if (match) {
                allowed.push(parseInt(match[1]));
            }

            match = lines[i].match(rtxRegex);
            if (match && allowed.includes(parseInt(match[2]))) {
                allowed.push(parseInt(match[1]));
            }
        }
    }

    var skipRegex = 'a=(fmtp|rtcp-fb|rtpmap):([0-9]+)';
    var sdp = '';

    isKind = false;
    for (var i = 0; i < lines.length; i++) {
        if (lines[i].startsWith('m=' + kind + ' ')) {
            isKind = true;
        } else if (lines[i].startsWith('m=')) {
            isKind = false;
        }

        if (isKind) {
            var skipMatch = lines[i].match(skipRegex);
            if (skipMatch && !allowed.includes(parseInt(skipMatch[2]))) {
                continue;
            } else if (lines[i].match(videoRegex)) {
                sdp += lines[i].replace(videoRegex, '$1 ' + allowed.join(' ')) + '\n';
            } else {
                sdp += lines[i] + '\n';
            }
        } else {
            sdp += lines[i] + '\n';
        }
    }

    return sdp;
}

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

document.addEventListener("DOMContentLoaded", () => {
    var volumeSlider = document.getElementById("volume-slider");
    const vol = localStorage.getItem('volume');
    if (volumeSlider && (vol || vol == 0.0)) {
        volumeSlider.value = vol;
    }
});