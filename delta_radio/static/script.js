const tuneSlider = document.getElementById('tune-slider');
const volSlider = document.getElementById('volume-slider');
const tune = document.getElementById('tune');
const player = document.getElementById('player');
const playBtn = document.getElementById('play');
const songMap = {};


function parseM3U8(data) {
    const lines = data.split('\n');
    let sumDRLEN = 0;

    for (const line of lines) {
        if (line.startsWith('https://')) {
            const url = line.trim();
            const match = url.match(/__DRLEN_(\d+)\.mp3/);
            if (match) {
                const drlen = parseInt(match[1]);
                songMap[sumDRLEN] = url;
                sumDRLEN += drlen;
            } else {
                console.warn('No DRLEN found for:', url);
            }
        }
    }

    console.log('Song map:', songMap);
    return songMap;
}


function findSongIndexAtTime(time) {
    const songTimes = Object.keys(songMap).map(Number);
    let index = 0;
    while (index < songTimes.length && time >= songTimes[index]) {
        index++;
    }
    return songTimes[index - 1];
}


function playMusic() {
    const currentTime = Math.floor(Date.now() / 1000) % Object.keys(songMap)[Object.keys(songMap).length - 1];
    const songIndex = findSongIndexAtTime(currentTime);
    const songURL = songMap[songIndex];
    const secondsToSeek = currentTime - songIndex;

    player.src = songURL;
    player.currentTime = secondsToSeek;
    player.play();
}

function playMusicAt(currentIdx) {
    const currentTime = currentIdx % Object.keys(songMap)[Object.keys(songMap).length - 1];
    const songIndex = findSongIndexAtTime(currentTime);
    const songURL = songMap[songIndex];
    const secondsToSeek = currentTime - songIndex;

    player.src = songURL;
    player.currentTime = secondsToSeek;
    player.play();
}


player.addEventListener('ended', () => {
    playMusic();
});


const changeTune = () => {
    let value = Number(tuneSlider.value);
    value = value.toFixed(1);
    tune.textContent = value + ' MHz';
}

tuneSlider.addEventListener('input', changeTune);

const getCurrentSeed = () => {
    let value = Number(tuneSlider.value);
    value = value.toFixed(1);
    return value.toString().replace(".", "");
}

playBtn.addEventListener('click', () => {
    playBtn.disabled = true;
    tuneSlider.disabled = true;

    url = `https://radio.deltadelta.dev/radio.m3u8?seed=${getCurrentSeed()}`

    console.log('Fetching M3U8 file:', url);

    fetch(url)
        .then(response => response.text())
        .then(data => parseM3U8(data))
        .then(playMusic)
        .catch(error => console.error('Error fetching M3U8 file:', error));
});

const changeVolume = () => {
    let value = volSlider.value;
    player.volume = value / 100;
}

volSlider.addEventListener('input', changeVolume);


changeVolume();
changeTune();
