import json
import logging
import os
import random

from aiohttp import web
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaBlackhole

from delta_radio import pcs, stream_player, stream_relay
from delta_radio.authentication import authenticate
from delta_radio.delta_radio_player import DeltaRadioMediaSource

logger = logging.getLogger("pc")
logging.basicConfig(level=logging.INFO)


ROOT = os.path.dirname(__file__)


def fetch_static_content(static_path: str):
    with open(os.path.join(ROOT, "static/", static_path)) as f:
        content = f.read()
    return content


async def index(request):
    return web.Response(
        content_type="text/html", text=fetch_static_content("index.html")
    )
    # if authenticate(request):
    #     return web.Response(
    #         content_type="text/html", text=fetch_static_content("index.html")
    #     )
    # else:
    #     raise web.HTTPUnauthorized(headers={"WWW-Authenticate": "Basic"})


async def javascript(request):
    return web.Response(
        content_type="application/javascript", text=fetch_static_content("script.js")
    )


async def style(request):
    return web.Response(content_type="text/css", text=fetch_static_content("style.css"))


async def offer(request):
    # Waiting until a fix for the memory leak
    return web.Response(status=501)

    params = await request.json()
    _offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])

    pc = RTCPeerConnection()
    pcs.add(pc)

    # All connections are tranceivers so we want to sink the data coming to the server
    sink = MediaBlackhole()

    proxy_player = stream_relay.subscribe(stream_player.audio)

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        if pc.connectionState in ("failed", "disconnected", "closed"):
            stream_relay._stop(proxy_player)
            await pc.close()
            pcs.discard(pc)

    @pc.on("track")
    def on_track(track):
        pc.addTrack(proxy_player)
        sink.addTrack(track)
        sink.start()

    # pc.addTrack(proxy_player)

    # handle offer
    await pc.setRemoteDescription(_offer)

    # send answer
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)

    return web.Response(
        content_type="application/json",
        text=json.dumps(
            {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}
        ),
    )


def generate_m3u8(request):
    seed = request.query.get("seed", None)
    player = DeltaRadioMediaSource(os.getenv("RADIO_MUSIC_DIR", "/"))
    tracks: list[str] = player.tracks
    m3u_segments: list[str] = []

    try:
        seed = int(seed)
        random.seed(seed)
    except (ValueError, TypeError):
        pass

    random.shuffle(tracks)

    # Ensure that an intermission comes first
    tracks.insert(0, player.get_random_intermission())

    for i, track in enumerate(tracks):
        # Redirect ot the absolute web root
        current_track = os.path.join(os.getenv("RADIO_CONTENT_HTTP_ROOT", ""), track)
        # Add m3u8 metadata
        current_track = f"#EXTINF:-1, Track {i}\n{current_track}"

        m3u_segments.append(current_track)

    tracks_payload = "\n".join(m3u_segments)

    return web.Response(
        content_type="audio/x-mpegurl",
        text=f"#EXTM3U\n{tracks_payload}\n#EXT-X-ENDLIST",
    )
