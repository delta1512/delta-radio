import base64
import os

import pyotp
from dotenv import load_dotenv

load_dotenv()


def authenticate(request) -> bool:
    auth_header = request.headers.get("Authorization")
    if not auth_header or not auth_header.startswith("Basic "):
        return False

    credentials = auth_header[6:]
    decoded_credentials = base64.b64decode(credentials).decode("utf-8")

    # Split the decoded credentials into username and TOTP code
    username, password = decoded_credentials.split(":")

    if username == os.getenv("OWNER_USERNAME"):
        return handle_owner_authentication(username, password)
    elif username == os.getenv("GUEST_USERNAME"):
        return handle_guest_authentication(username, password)
    else:
        return False


def handle_owner_authentication(username: str, password: str) -> bool:
    return username == os.getenv("OWNER_USERNAME") and password == os.getenv(
        "OWNER_PASSWORD"
    )


def handle_guest_authentication(username: str, totp_code: str) -> bool:
    if not username == os.getenv("GUEST_USERNAME"):
        return False

    totp_secret = os.getenv("GUEST_TOTP_SECRET")
    totp = pyotp.TOTP(totp_secret)

    return bool(totp.verify(totp_code, valid_window=60 * 60))
