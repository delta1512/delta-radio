#!/bin/bash


# Takes an input audio file (arg1) and converts it to the output file (arg 2) ensuring it complies with delta radio standards

# SECONDS_F=$(soxi -D "$1")
# SECONDS=$(echo "($SECONDS_F - 1) / 1" | bc)

# BASE=$(basename -s .wav "$1")
# BASE=$(basename "$2")

#ffmpeg -i "$1" -c:a copy -to "$SECONDS" "$2"

ffmpeg -i "$1" -ar 48000 -sample_fmt s16 "$2"

#echo $FRAMES
#echo $SECONDS

# for i in delta_radio/*.wav; do /home/mark/prog/delta-radio/wavfix.sh $i ${i/.wav/.flac}; done;

# for i in *.flac; do /home/mark/prog/delta-radio/flacfix.sh "$i" "../delta_radio_fixed/$i"; done;


# for file in *.mp3; do mv -n "$file" "$(echo "$file" | sed 's/[^_a-zA-Z0-9\.]/_/g')"; done

