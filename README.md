# Hi there

This is Delta radio, my own personal web radio.

I'll add some more documents here when I have things better set up but at the moment this is just me playing around with things.

### Internal notes

Run with: `docker compose up`

Then connect to it with `172.16.61.2:8000/radio.m3u8`

Download a YT vid with multiple sections:


```
mp3yt --download-sections "^\d\d.*" -o "%(title)s-%(section_title)s.%(ext)s" "https://www.youtube.com/watch?v=12345"
```
